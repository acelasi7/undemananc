FROM ruby:2.6.5-slim-buster

RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -qq -y --no-install-recommends \
  build-essential \
  # postgresql
  libpq-dev


RUN mkdir /app
WORKDIR /app

# Caching `bundle install`
COPY Gemfile Gemfile.lock ./
RUN bundle install --system --quiet --no-cache --jobs=3

COPY . .

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
