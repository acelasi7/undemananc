
# undemananc

## script/function

- `ruby find_my_lunch.rb ./data/restaurants.csv 2019-10-12T20:12`

## API

- Ruby on Rails
- `docker-compose up`
- [localhost:3000](http://localhost:3000)
- for seeding `docker-compose run undemananc sh -c "rake restaurants:load_from_csv CSV_FILE="./data/restaurants.csv"`

### Endpoints

- GET    /reservations
- POST   /reservations
- GET    /reservations/:id
- PATCH  /reservations/:id
- PUT    /reservations/:id
- DELETE /reservations/:id
- GET    /restaurants/:restaurant_id/reservations
- POST   /restaurants/:restaurant_id/reservations
- GET    /restaurants
- POST   /restaurants
- GET    /restaurants/:id
- PATCH  /restaurants/:id
- PUT    /restaurants/:id

### Examples

- GET    /restaurants?open_at=2019-10-19T23:40
```json
[
    {
        "id": 6,
        "name": "The Cheesecake Factory",
        "vegan_friendly": false,
        "children_friendly": false,
        "seats": 3,
        "schedule": "Mon-Thu 11 am - 11 pm  / Fri-Sat 11 am - 12:30 am  / Sun 10 am - 11 pm",
        "created_at": "2019-10-20T20:18:50.289Z",
        "updated_at": "2019-10-20T20:18:50.289Z"
    }
]
```

- POST /restaurants
```json
{
	"name": "Lodan2",
	"vegan_friendly": true,
	"children_friendly": false,
	"seats": 21,
	"schedule": "Thu-Fri 12 pm - 10 pm / Sat 6 pm - 12 am"
}
```
```json
{ "Content-Type": "application/json" }
```
```json
{

"id":  53,

"name":  "Lodan2",

"vegan_friendly":  true,

"children_friendly":  false,

"seats":  21,

"schedule":  "Thu-Fri 12 pm - 10 pm / Sat 6 pm - 12 am",

"created_at":  "2019-10-20T21:18:15.716Z",

"updated_at":  "2019-10-20T21:18:15.716Z"

}
```
- PATCH /restaurants/53
```json
{
	"name": "Lodan2bis",
	"seats": 50,
	"schedule": "Thu-Fri 12 pm - 10 pm"
}
```
```json
{ "Content-Type": "application/json" }
```
```json

{

"name":  "Lodan2bis",

"seats":  50,

"schedule":  "Thu-Fri 12 pm - 10 pm",

"id":  53,

"vegan_friendly":  true,

"children_friendly":  false,

"created_at":  "2019-10-20T21:18:15.716Z",

"updated_at":  "2019-10-20T22:06:23.948Z"

}
```

- POST /restaurants/1/reservations
```json
{
    "name": "2 happy friends",
    "seats": 2,
    "start_at": "2019-10-21T14:00Z",
    "end_at": "2019-10-21T15:00Z"
}
```
```json
{ "Content-Type": "application/json" }
```
```json

{

"id":  1,

"restaurant_id":  1,

"name":  "2 happy friends",

"seats":  2,

"start_at":  "2019-10-21T14:00:00.000Z",

"end_at":  "2019-10-21T15:00:00.000Z",

"created_at":  "2019-10-20T23:25:44.311Z",

"updated_at":  "2019-10-20T23:25:44.311Z"

}
```
etc.


### Limitations

- !!! anyone can update any restaurant !!!
- !!! anyone can delete/update any reservation !!!
- pagination is missing
- no support for opening/closing times that are not weekly
- lacking tests on the API
- default serializers
