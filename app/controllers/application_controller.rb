class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: -> { render_error status: :not_found }

  def render_error(message: nil, status:)
    status_code = Rack::Utils::SYMBOL_TO_STATUS_CODE[status]
    title = Rack::Utils::HTTP_STATUS_CODES[status_code]
    render json: { errors: [{ message: message, status: status_code.to_s, title: title }] },
           status: status
  end
end
