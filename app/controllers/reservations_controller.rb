# frozen_string_literal: true

class ReservationsController < ApplicationController
  def index
    reservations = Reservation
      .all
      .tap { |it| it.where(restaurant_id: params[:restaurant_id]) if params[:restaurant_id] }

    render json: reservations, status: :ok
  end

  def show
    reservation = Reservation.find(params[:id])
    render json: reservation, status: :ok
  end

  def create
    reservation = Reservation.new(permitted_params)

    if reservation.save
      render json: reservation, status: :created
    else
      render_error message: reservation.errors.full_messages, status: :unprocessable_entity
    end
  end

  def update
    debugger
    reservation = Reservation.find(params[:id])
                             .tap { |it| it.assign_attributes(permitted_params) }

    if reservation.save
      render json: reservation, status: :ok
    else
      render_error message: reservation.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    reservation = Reservation.find(params[:id])

    if reservation.destroy
      render json: reservation, status: :ok
    else
      render_error message: reservation.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def validate_params
    datetime = DateTime.iso8601(params[:start_at]) if params[:start_at]
  rescue ArgumentError => e
    render_error message: 'invalid date' ,status: :unprocessable_entity
  end

  def permitted_params
    params.permit(:restaurant_id, :name, :seats, :start_at, :end_at)
  end
end
