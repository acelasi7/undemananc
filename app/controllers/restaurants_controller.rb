# frozen_string_literal: true

require 'date_time_schedule'

class RestaurantsController < ApplicationController
  before_action :validate_filters, only: :index

  def index
    # TODO pagination
    restaurants = Restaurant
                  .all
                  .then { |it| filter_by_opening_time(it) }

    render json: restaurants, status: :ok
  end

  def show
    restaurant = Restaurant.find(params[:id])
    render json: restaurant, status: :ok
  end

  def create
    restaurant = Restaurant.new(permitted_params)
    DateTimeSchedule::Weekly.parse(restaurant.schedule).minute_ranges do |minute_range|
      restaurant.weekly_opening_times << WeeklyOpeningTime.new(
        open_at_week_minute: minute_range.first,
        close_at_week_minute: minute_range.last
      )
    end

    if restaurant.save
      render json: restaurant, status: :created
    else
      render_error message: restaurant.errors.full_messages, status: :unprocessable_entity
    end
  end

  def update
    restaurant = Restaurant.find(params[:id])
                           .tap { |it| it.assign_attributes(permitted_params) }
    if restaurant.schedule_changed?
      restaurant.weekly_opening_times = []
      DateTimeSchedule::Weekly.parse(restaurant.schedule).minute_ranges do |minute_range|
        restaurant.weekly_opening_times << WeeklyOpeningTime.new(
          open_at_week_minute: minute_range.first,
          close_at_week_minute: minute_range.last
        )
      end
    end

    if restaurant.save
      render json: restaurant, status: :ok
    else
      render_error message: restaurant.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def filter_by_opening_time(restaurants)
    return restaurants unless params[:open_at]

    datetime = DateTime.iso8601(params[:open_at])
    week_minute = (datetime.to_time - DateTimeSchedule::Weekly.beginning_of_week).div(60).floor
    restaurants.joins(:weekly_opening_times)
               .merge(WeeklyOpeningTime.open_at(week_minute))
  end

  def validate_filters
    datetime = DateTime.iso8601(params[:open_at]) if params[:open_at]
  rescue ArgumentError => e
    render_error message: 'invalid date' ,status: :unprocessable_entity
  end

  def permitted_params
    params.permit(:name, :vegan_friendly, :children_friendly, :seats, :schedule)
  end
end
