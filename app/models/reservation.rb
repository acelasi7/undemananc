# frozen_string_literal: true

class Reservation < ApplicationRecord
  # TODO validations, we're relying on the db for now

  belongs_to :restaurant

  validate :restaurant_is_available

  private

  def restaurant_is_available
    start_at_week_minute = DateTimeSchedule::Weekly.minute_of_week(start_at)
    opening_time = restaurant
                   .weekly_opening_times
                   .open_at(start_at_week_minute)
                   .first
    errors.add(:restaurant, "#{restaurant.name} is not available at this time") && return unless opening_time.present?

    minutes_until_closed = opening_time.close_at_week_minute % DateTimeSchedule::Weekly::MINUTES_IN_A_WEEK - start_at_week_minute
    enough_minutes = minutes_until_closed >= (end_at - start_at).div(60).floor
    errors.add(:restaurant, "#{restaurant.name} closes earlier than the end of your reservation") && return unless enough_minutes

    overlapping_reservations = restaurant.reservations
      .where(['start_at <= ? AND end_at > ?', start_at, start_at])
      .or(self.class.where(['start_at <= ? AND end_at > ?', end_at, end_at]))
      .where.not(id: id)
    enough_seats = restaurant.seats >= overlapping_reservations.sum(:seats) + seats
    errors.add(:restaurant, "#{restaurant.name} doesn't have enough seats at this time") && return unless enough_seats
  end
end
