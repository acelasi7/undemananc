# frozen_string_literal: true

class Restaurant < ApplicationRecord
  # TODO validations, we're relying on the db for now

  has_many :reservations, dependent: :nullify
  has_many :weekly_opening_times, dependent: :destroy
end
