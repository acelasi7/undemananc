# frozen_string_literal: true

require 'date_time_schedule'

class WeeklyOpeningTime < ApplicationRecord
  # TODO validations, we're relying on the db for now

  belongs_to :restaurant

  scope :open_at, ->(week_minute) {
    next_week_minute = week_minute + DateTimeSchedule::Weekly::MINUTES_IN_A_WEEK
    where([
      "open_at_week_minute <= ? AND close_at_week_minute > ?",
      week_minute,
      week_minute
    ])
    .or(all.where([
      "open_at_week_minute <= ? AND close_at_week_minute > ?",
      next_week_minute,
      next_week_minute
    ]))
  }
end
