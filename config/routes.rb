Rails.application.routes.draw do
  scope defaults: { format: :json } do
    resources :reservations
    resources :restaurants, except: :destroy do
      resources :reservations, only: [:show, :create]
    end
  end
end
