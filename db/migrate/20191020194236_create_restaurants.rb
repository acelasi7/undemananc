class CreateRestaurants < ActiveRecord::Migration[6.0]
  def change
    create_table :restaurants do |t|
      t.string :name, null: false
      t.boolean :vegan_friendly, null: false, default: false
      t.boolean :children_friendly, null: false, default: false
      t.integer :seats, null: false, default: 0
      t.string :schedule, null: false, default: ''

      t.timestamps null: false
    end
  end
end
