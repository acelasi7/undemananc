class CreateWeeklyOpeningTimes < ActiveRecord::Migration[6.0]
  def change
    create_table :weekly_opening_times do |t|
      t.references :restaurant, index: true, foreign_key: true, null: false
      t.integer :open_at_week_minute, null: false
      t.integer :close_at_week_minute, null: false

      t.timestamps null: false
    end
  end
end
