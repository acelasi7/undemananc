class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.references :restaurant, index: true, foreign_key: true
      t.string :name, null: false
      t.integer :seats, null: false, default: 1
      t.datetime :start_at, null: false
      t.datetime :end_at, null: false

      t.timestamps null: false
    end
  end
end
