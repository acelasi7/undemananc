require 'csv'
require 'date'

require './lib/date_time_schedule'

def find_my_lunch(csv_file, datetime)
  restaurants = CSV.foreach(csv_file, headers: true).select do |row|
    DateTimeSchedule::Weekly.parse(row['Schedule']).at?(datetime)
  end.map(&:to_h)

  puts ""
  puts "At: #{datetime.iso8601}"
  puts "============"
  puts "Restaurants:"
  puts ""
  restaurants.each do |restaurant|
    puts <<~STRING
      #{restaurant['Name']}
      vegan #{restaurant['Vegan option'] == 'yes' ? '👍' : '👎' }
      age #{restaurant['Child friendly'] == 'yes' ? '0+' : '16+' }
      seats #{restaurant['Number of seats']}
      #{restaurant['Schedule']}

    STRING
  end
  puts "============"
  puts ""
end

def datetime_iso8601?(string)
  DateTime.iso8601(string)
  true
rescue ArgumentError
  false
end

# ###

def main
  abort('Should be called "ruby find_my_lunch.rb <csv_file_path> <datetime_iso8601>"') if ARGV.length != 2
  file, datetime = ARGV[0..1]
  abort('Should provide a valid csv file') unless File.exist?(file)
  abort('Should provide a valid iso8601 datetime') unless datetime_iso8601?(datetime)

  find_my_lunch(file,  DateTime.iso8601(datetime))
end

main
