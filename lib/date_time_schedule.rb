require 'date'

module DateTimeSchedule
  class Error < StandardError; end

  class Weekly
    def self.beginning_of_week(datetime = DateTime.now)
      first_day_of_the_week = 0 # Date.parse('Sun').wday
      date = datetime.to_date
      (date - (date.wday - first_day_of_the_week) % 7).to_datetime
    end

    # from 0 to 10_079
    def self.minute_of_week(datetime = DateTime.now)
      (datetime.to_time - beginning_of_week(datetime).to_time).div(60).floor
    end

    def self.beginning_of_day(datetime = DateTime.now)
      datetime.to_date.to_datetime
    end

    def self.minute_of_day(datetime = DateTime.now)
      (datetime.to_time - beginning_of_day(datetime).to_time).div(60).floor
    end

    def self.validate_format!(string)
      if validate_format(string)
        true
      else
        raise Error, "unsupported schedule format! string: #{string}"
      end
    end

    def self.validate_format(string)
      pattern = /\A#{entry_pattern_string}(( )*\/( )* #{entry_pattern_string})*\z/
      pattern.match?(string)
    end

    def self.entry_pattern_string
      day_of_the_week ='((Mon)|(Tue)|(Wed)|(Thu)|(Fri)|(Sat)|(Sun))'
      day_range = "#{day_of_the_week}(-#{day_of_the_week})?"
      day_pattern = "#{day_range}(, #{day_range})?"
      time = '\d{1,2}(:\d{1,2})?(( am)|( pm))?'
      time_range = "#{time}( - #{time})?"

      "(?<days>#{day_pattern})( (?<times>#{time_range}))?"
    end

    def self.parse(string)
      validate_format!(string)
      pattern = /\A#{entry_pattern_string}\z/

      entries = string.split(' / ').map(&:strip).map do |entry|
        matches = pattern.match(entry)
        parse_days(matches[:days])
          .merge(parse_times(matches[:times]))
        end

      new(entries)
    end

    # returned day is an integer from 0 to 6, where 0 is Sunday, 1 is Monday etc
    DAYS_IN_A_WEEK = 7
    def self.parse_days(days)
      days.split(',').map do |day_range|
      day_range
          .split('-')
          .map { |it| Date.parse(it) }
          .map { |it| it.wday }
          .then { |it| it.size == 1 ? it * 2 : it }
          .then { |it| it[0] > it[1] ? [it[0], it[1] + DAYS_IN_A_WEEK] : it }
          .then { |it| (it[0]..it[1]).to_a }
      end.reduce(:+)
         .then { |it| { days: it } }
    end

    # returned time is a non-negative integer, which counts minutes
    def self.parse_times(times)
      times
        .split(' - ')
        .map { |it| DateTime.parse(it) }
        .map { |it| minute_of_day(it) }
        .then { |it| { start_time: it[0], end_time: it[1] } }
    end

    def initialize(entries)
      @entries = entries
    end


    # NOTE: not always true, eg. day time saving times
    MINUTES_IN_A_DAY = 1_440
    MINUTES_IN_A_WEEK = 10_080
    def at?(datetime)
      @entries.any? do |entry|
        days = entry[:days]
        start_time = entry[:start_time]
        end_time = entry[:end_time].then { |it| it >= start_time ? it : it + MINUTES_IN_A_DAY }

        days.any? do |day|
          start_minute = day * MINUTES_IN_A_DAY + start_time
          end_minute = day * MINUTES_IN_A_DAY + end_time
          # NOTE: we exclude last minute, assuming the schedule interval is exclusive on the right hand side
          opened_minutes = (start_minute...end_minute)

          # NOTE: end_minute can overflow the MINUTES_IN_A_WEEK;
          #       we allow it and prefer to overflow the datetime_minute for checks,
          #       this way we preserve the linearity of time and simulate the cycle of the week
          datetime_minute = self.class.minute_of_week(datetime)
          opened_minutes.include?(datetime_minute) \
            || opened_minutes.include?(datetime_minute + MINUTES_IN_A_WEEK)
        end
      end
    end

    def minute_ranges
      @entries.each do |entry|
        days = entry[:days]
        start_time = entry[:start_time]
        end_time = entry[:end_time].then { |it| it >= start_time ? it : it + MINUTES_IN_A_DAY }
        days.each do |day|
          start_minute = day * MINUTES_IN_A_DAY + start_time
          end_minute = day * MINUTES_IN_A_DAY + end_time
          yield (start_minute...end_minute)
        end
      end
    end
  end
end
