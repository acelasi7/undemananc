# frozen_string_literal: true

require 'csv'

require './lib/date_time_schedule'

namespace :restaurants do
  desc 'Load restaurants from CSV'
  task load_from_csv: :environment do
    csv_file = ENV.fetch('CSV_FILE')

    restaurants = CSV.foreach(csv_file, headers: true).select do |row|
      restaurant = Restaurant.new(
        name: row['Name'],
        vegan_friendly: row['Vegan option'] == 'yes',
        children_friendly: row['Children friendly'] == 'yes',
        seats: row['Number of seats'].to_i,
        schedule: row['Schedule']
      )
      DateTimeSchedule::Weekly.parse(restaurant.schedule).minute_ranges do |minute_range|
        restaurant.weekly_opening_times << WeeklyOpeningTime.new(
          open_at_week_minute: minute_range.first,
          close_at_week_minute: minute_range.last
        )
      end
      restaurant.save!
    end
  end
end
