require 'date'

require 'date_time_schedule'

RSpec.describe DateTimeSchedule::Weekly do
  describe 'validate_format' do
    context 'when it\'s valid' do
      [
        'Mon',
        'Mon 11 am - 9 pm',
        'Mon-Tue',
        'Mon-Tue 11 am - 9 pm',
        'Mon-Tue 11:30 am - 9 pm',
        'Mon-Tue 11:30 am - 9:21 pm',
        'Mon-Tue 11:30 - 21:21',
        'Mon, Fri 8 am - 10 am',
        'Mon, Fri-Sat 8 am - 10 am',
        'Mon, Fri-Sat 8:00 am - 10:30 pm',
      ].each do |schedule|
        it "#{schedule}" do
          validity = DateTimeSchedule::Weekly.validate_format(schedule)
          expect(validity).to eq true
        end
      end
    end

    context 'when it\'s invalid' do
      [
        'Mons',
        'Mon 11am - 9 pm',
        'Mons-Tue',
        'Mon-Tue 11am - 9 pm',
        'Mon-Tue 11 am-9 pm',
        'Mon-Tue 1130 am - 9 pm',
        'Mon-Tue 11:30 am - 921 pm',
        'Mon, 8 am - 10 am',
      ].each do |schedule|
        it "#{schedule}" do
          validity = DateTimeSchedule::Weekly.validate_format(schedule)
          expect(validity).to eq false
        end
      end
    end
  end

  describe 'validate_format!' do
    context 'when schedule format is valid' do
      it 'returns true' do
        expect(DateTimeSchedule::Weekly).to receive(:validate_format).and_return(true)
        expect(DateTimeSchedule::Weekly.validate_format!('some-string')).to eq true
      end
    end

    context 'when schedule format is invalid' do
      it 'raises error' do
        expect(DateTimeSchedule::Weekly).to receive(:validate_format).and_return(false)
        expect {
          DateTimeSchedule::Weekly.validate_format!('some-string')
        }.to raise_error(DateTimeSchedule::Error)
      end
    end
  end

  describe '.at?' do
    context 'when it\'s closed' do
      [
        # before
        ['Mon-Sun 11:30 am - 9 pm', '2019-10-21T11:00'],
        ['Mon 11:30 am - 9 pm', '2019-10-21T11:00'],
        ['Mon 11:30 am - 9 pm', '2019-10-21T11:29:59'],
        # after
        ['Mon 11:30 am - 9 pm', '2019-10-21T21:00'],
        ['Mon 11:30 am - 9 pm', '2019-10-21T21:00:01'],
        ['Mon 11:30 am - 9 pm', '2019-10-21T21:01'],
        # comma separated days
        ['Mon, Fri-Sat 8 am - 10 am', '2019-10-22T09:00'],
        ['Mon, Fri-Sat 8 am - 10 am', '2019-10-24T09:00'],
        ['Mon, Fri-Sat 8 am - 10 am', '2019-10-27T09:00'],
        # forward slash separated entries
        ['Mon, Fri-Sat 8 am - 10 am / Tue-Wed 1 pm - 3 pm', '2019-10-22T09:00'],
        ['Mon-Wed 5 pm - 12:30 am / Thu-Fri 5 pm - 1:30 am / Sat 3 pm - 1:30 am / Sun 3 pm - 11:30 pm', '2019-10-21T00:40'],
      ].each do |schedule, datetime_of_interest|
        it "#{schedule}, #{datetime_of_interest}"do
          schedule = DateTimeSchedule::Weekly.parse(schedule)
          datetime = DateTime.parse(datetime_of_interest)
          expect(schedule.at?(datetime)).to eq false
        end
      end
    end

    context 'when it\'s open' do
      [
        # on first day
        ['Mon-Sun 11:30 am - 9 pm', '2019-10-21T12:00'],
        # on last day
        ['Mon-Sun 11:30 am - 9 pm', '2019-10-20T12:00'],
        # right at the beginning
        ['Mon 11:30 am - 11:31 am', '2019-10-21T11:30'],
        ['Mon 11:30 am - 11:31 am', '2019-10-21T11:30:01'],
        # right at the end
        # ['Mon 11:30 am - 11:31 am', '2019-10-21T11:31'],
        ['Mon 11:30 am - 11:31 am', '2019-10-21T11:30:59'],
        # meridian notation vs military notation
        ['Mon 11:30 am - 9 pm', '2019-10-21T20:59'],
        # on the second day morning
        ['Mon-Sun 11:30 am - 9 am', '2019-10-21T08:00'],
        # comma separated days
        ['Mon, Fri-Sat 8 am - 10 am', '2019-10-21T09:00'],
        ['Mon, Fri-Sat 8 am - 10 am', '2019-10-25T09:00'],
        ['Mon, Fri-Sat 8 am - 10 am', '2019-10-26T09:00'],
        # forward slash separated entries
        ['Mon, Fri-Sat 8 am - 10 am / Tue-Wed 1 pm - 3 pm', '2019-10-25T09:00'],
        ['Mon, Fri-Sat 8 am - 10 am / Tue-Wed 1 pm - 3 pm', '2019-10-22T14:00'],
        ['Mon-Wed 5 pm - 12:30 am / Thu-Fri 5 pm - 1:30 am / Sat 3 pm - 1:30 am / Sun 3 pm - 11:30 pm', '2019-10-20T00:40'],
      ].each do |schedule, datetime_of_interest|
        it "#{schedule}, #{datetime_of_interest}"do
          schedule = DateTimeSchedule::Weekly.parse(schedule)
          datetime = DateTime.parse(datetime_of_interest)
          expect(schedule.at?(datetime)).to eq true
        end
      end
    end
  end

  describe '.beginning_of_day' do
    it 'works' do
      datetime = DateTimeSchedule::Weekly.beginning_of_day(DateTime.parse('2019-10-20T11:00'))
      expect(datetime).to eq DateTime.parse('2019-10-20T00:00:00')
    end
  end

  describe '.beginning_of_week' do
    it 'works' do
      datetime = DateTimeSchedule::Weekly.beginning_of_week(DateTime.parse('2019-10-19T11:00'))
      expect(datetime).to eq DateTime.parse('2019-10-13T00:00:00')
    end
  end

  describe '.minute_of_day' do
    it 'works' do
      minute = DateTimeSchedule::Weekly.minute_of_day(DateTime.parse('2019-10-22T11:00'))
      expect(minute).to eq 660
    end
  end

  describe '.minute_of_week' do
    it 'works' do
      minute = DateTimeSchedule::Weekly.minute_of_week(DateTime.parse('2019-10-21T11:00'))
      expect(minute).to eq 2100
    end
  end
end
